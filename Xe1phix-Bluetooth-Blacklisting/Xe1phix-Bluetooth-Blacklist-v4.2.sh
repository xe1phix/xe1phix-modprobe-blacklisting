# Prevent Bluetooth autoload 
echo "alias net‐pf‐31 off" >> /etc/modprobe.d/bluetooth-blacklist.conf
echo "install bluetooth /bin/false" >> /etc/modprobe.d/bluetooth-blacklist.conf
echo "install net-pf-31 /bin/false" >> /etc/modprobe.d/bluetooth-blacklist.conf

echo "blacklist btusb" > /etc/modprobe.d/bluetooth-blacklist.conf
echo "blacklist bluetooth" > /etc/modprobe.d/bluetooth-blacklist.conf





## ---------------------------------------------------------------------------------------- ##
alias net-pf-31 off
blacklist bluetooth
remove bluetooth

options disable_esco=1       ## disable_esco:Disable eSCO connection creation (bool)
options disable_ertm=1       ## disable_ertm:Disable enhanced retransmission mode (bool)
## ---------------------------------------------------------------------------------------- ##
blacklist btusb
remove btusb
## ---------------------------------------------------------------------------------------- ##
blacklist btsdio                  ## Generic Bluetooth SDIO driver ver 0.1
remove btsdio                  ## Generic Bluetooth SDIO driver ver 0.1
## ---------------------------------------------------------------------------------------- ##
blacklist btintel                 ## Bluetooth support for Intel devices ver 0.1
remove btintel                 ## Bluetooth support for Intel devices ver 0.1
## ---------------------------------------------------------------------------------------- ##
blacklist btrtl                   ## Bluetooth support for Realtek devices ver 0.1
remove btrtl                   ## Bluetooth support for Realtek devices ver 0.1
## ---------------------------------------------------------------------------------------- ##
blacklist bt3c_cs                 ## Bluetooth driver for the 3Com Bluetooth PCMCIA card
remove bt3c_cs                 ## Bluetooth driver for the 3Com Bluetooth PCMCIA card
## ---------------------------------------------------------------------------------------- ##
blacklist btmrvl                  ## description:    Marvell Bluetooth driver ver 1.0
remove btmrvl                  ## description:    Marvell Bluetooth driver ver 1.0
## ---------------------------------------------------------------------------------------- ##
blacklist btqca                   ## Bluetooth support for Qualcomm Atheros family ver 0.1
remove btqca                   ## Bluetooth support for Qualcomm Atheros family ver 0.1
## ---------------------------------------------------------------------------------------- ##
alias net-pf-31 off
blacklist btbcm                   ## Bluetooth support for Broadcom devices ver 0.1
remove btbcm
## ---------------------------------------------------------------------------------------- ##
blacklist bluetooth_6lowpan                   ## Bluetooth 6LoWPAN
remove bluetooth_6lowpan
## ---------------------------------------------------------------------------------------- ##
blacklist rfcomm               ## Bluetooth RFCOMM ver 1.11
remove rfcomm
options disable_cfc 1          ## :Disable credit based flow control
## ---------------------------------------------------------------------------------------- ##

